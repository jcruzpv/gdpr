<?php

echo esc_html__(
'<p>We resolved your rectification request.</p>
<p>If you have any problems or questions, don\'t hesitate to contact us.</p>', 'gdpr' );
