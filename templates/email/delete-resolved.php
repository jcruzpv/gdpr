<?php

echo sprintf(
	/* translators: 6-digit token for audit log */
  esc_html__(
'<p>Your account has been closed.</p>

<p>We no longer hold any information about you.</p>
<p>If you ever need to make a complaint you can email us and we will try to help you.</p>
<p>To be able to make a complaint you will be requested to provide your email address and the token below.</p>

%s', 'gdpr' ),
  $args['token']
);
