<?php
echo sprintf(
  /* translators: 1: The complaint content, 2: confirmation link, 3: reset password link */
  esc_html__(
'<p>Someone placed a complaint on your behalf on our site.</p>
<p>By clicking confirm a request will be made and we will do our best to fulfil it.</p>
<p>--------------------------------------------------------</p>
<p>Request</p>
<p>--------------------------------------------------------</p>
%s
<p>To confirm this request, click here: %s </p>
<p>---------------------------------------------------------------------------------</p>
<p>If that wasn\'t you, reset your password: %s </p>
', 'gdpr' ),
  esc_html( $args['data'] ),
  esc_url_raw( $args['confirm_url'] ),
  esc_url_raw( $args['forgot_password_url'] )
);
