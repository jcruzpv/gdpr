<?php
echo sprintf(
  /* translators: 1: Confirmation link, 2: Reset password link */
  esc_html__(
'<p>Someone placed a request for your information to be removed from our site.</p>
<p>By clicking confirm your account will be removed from our site and all data we collected</p>
<p>over time will be erased from our database. It will be impossible for us to retrieve that</p>
<p>information in the future.</p>



<p>To confirm this request, click here: %s</p>



<p>---------------------------------------------------------------------------------</p>
<p>If that wasn\'t you, reset your password: %s </p>
', 'gdpr' ),
  esc_url_raw( $args['confirm_url'] ),
  esc_url_raw( $args['forgot_password_url'] )
);
