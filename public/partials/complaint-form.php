<form class="gdpr-add-to-complaint-requests" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
	<h3 class="mb-3">Complaint Form</h3>
	<?php wp_nonce_field( 'gdpr-add-to-requests', 'gdpr_request_nonce' ); ?>
	<input type="hidden" name="action" value="gdpr_send_request_email">
	<input type="hidden" name="type" value="complaint">
	<?php if ( ! is_user_logged_in() ): ?>
	<div class="form-group">
		<input class="form-control" type="email" name="user_email" placeholder="Email Address" required>
	</div>
	<?php endif ?>
	<div class="form-group">
		<textarea placeholder="Message..." class="form-control"  name="data" rows="5" required></textarea>
	</div>
	<?php GDPR_Public::add_recaptcha(); ?>
	<?php $submit_button_text = apply_filters( "gdpr_complaint_request_form_submit_text", esc_attr__( 'Submit', 'gdpr' ) ); ?>
	<div class="form-group text-right">
		<input class="btn btn-primary" type="submit" value="<?php echo esc_attr( $submit_button_text ); ?>">
	</div>
</form>
