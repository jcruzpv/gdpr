<?php
/**
 * The add to deletion requests form markup.
 *
 * @link       https://trewknowledge.com
 * @since      1.0.0
 *
 * @package    GDPR
 * @subpackage public
 * @author     Fernando Claussen <fernandoclaussen@gmail.com>
 */
?>

<form class="gdpr-add-to-deletion-requests" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
	<h3 class="mb-3">Request To Delete Data</h3>
	<?php wp_nonce_field( 'gdpr-add-to-requests', 'gdpr_request_nonce' ); ?>
	<input type="hidden" name="action" value="gdpr_send_request_email">
	<input type="hidden" name="type" value="delete">
	<?php if (! is_user_logged_in()){ ?>
		<div class="input-group mb-3">
		 <input class="form-control" type="email" name="user_email" placeholder="Email Address" required>
		  <div class="input-group-append">
			<button class="rounded-right btn btn-primary">Request To Delete Data</button>
		  </div>
		</div>
	<?php } ?>
	<?php GDPR_Public::add_recaptcha(); ?>
	<?php $submit_button_text = apply_filters( "gdpr_delete_request_form_submit_text", esc_attr__( 'Close my account', 'gdpr' ) ); ?>
	<?php if (is_user_logged_in() ){ ?>
		<input class="btn btn-primary" type="submit" value="<?php echo esc_attr( $submit_button_text ); ?>">
	<?php } ?>	
</form>
